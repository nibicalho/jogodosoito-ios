//
//  GameViewController.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 01/12/20.
//

import Foundation
import UIKit

class GameViewController: UIViewController {

    // MARK: - @IBOutlet
    @IBOutlet weak var switchSearchMethod: UISwitch!
    @IBOutlet weak var labelBFS: UILabel!
    @IBOutlet weak var labelIDA: UILabel!
    @IBOutlet weak var labelMoves: UILabel!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var boardView: GameBoardView!
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var buttonRestart: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Variables
    private var renderer: GameBoardRenderer?
    private let gameManager = LogicManager()
    private var isShuffling = false
    private var isAISolving = false
    private var totalMoves = 0
    private var currentMoves = 0
    weak var timer: Timer?
    private var startTime: Double = 0
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        renderer = GameBoardRenderer(boardView: boardView)
        gameManager.delegate = self
        gameManager.start()
    }
    
    // MARK: - Private
    func updateTimer() {
        startTime = Date().timeIntervalSince1970
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(statusTimer), userInfo: nil, repeats: true)
    }
    
    @objc func statusTimer() {
        let time = Date().timeIntervalSince1970 - startTime
        labelTimer.text = time.stringFromTimeInterval()
    }
    
    private func layoutStatus(_ enable: Bool) {
        spinner.isHidden = enable
        buttonStart.isEnabled = enable
        buttonRestart.isEnabled = enable
        switchSearchMethod.isUserInteractionEnabled = enable
        DispatchQueue.main.async {
            if !enable {
                self.spinner.startAnimating()
            } else {
                self.spinner.stopAnimating()
            }
        }
    }
    
    // MARK: - @IBAction
    @IBAction func tapStart(_ sender: Any) {
        gameManager.performAIFinish(isIDA: switchSearchMethod.isOn)
    }
    
    @IBAction func tapRestart(_ sender: Any) {
        labelTimer.text = "00 : 00 : 00"
        labelMoves.text = "00"
        totalMoves = 0
        currentMoves = 0
        timer = nil
        gameManager.start()
    }
    @IBAction func tapSwitchSearchMethod(_ sender: Any) {
        if switchSearchMethod.isOn {
            labelIDA.textColor = .systemBlue
            labelBFS.textColor = .black
        } else {
            labelIDA.textColor = .black
            labelBFS.textColor = .systemBlue
        }
    }
}

// MARK: - GameBoardViewDelegate
extension GameViewController: LogicManagerDelegate {
    
    func didAIStart() {
        updateTimer()
    }
    
    func didAISolve(moves: Int) {
        timer?.invalidate()
        totalMoves = moves
    }
    
    func didEnableInteractions() {
        layoutStatus(true)
        buttonStart.isEnabled = false
    }
    
    func didDisableInteractions() {
        layoutStatus(false)
    }
    
    func didStartShuffle() {
        boardView.alpha = 0.3
        layoutStatus(false)
    }
    
    func didEndShuffle() {
        boardView.alpha = 1.0
        layoutStatus(true)
    }
    
    func didSetTiles(tiles: [Tile]) {
        renderer?.setTiles(tiles: tiles)
    }
    
    func didSwapTiles(firstTile: Tile, secondTile: Tile, speed: TimeInterval, completionBlock: @escaping () -> Void) {
        renderer?.swapTiles(firstTile: firstTile, secondTile: secondTile, speed: speed, completionBlock: completionBlock)
        if totalMoves > 0 && currentMoves < totalMoves {
            currentMoves += 1
            labelMoves.text = String(format: "%02d", currentMoves)
        }
    }
}


