//
//  LogicManager.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 05/12/20.
//

import Foundation
import UIKit

protocol LogicManagerDelegate {
    func didAIStart()
    func didAISolve(moves: Int)
    func didEnableInteractions()
    func didDisableInteractions()
    func didStartShuffle()
    func didEndShuffle()
    func didSetTiles(tiles: [Tile])
    func didSwapTiles(firstTile: Tile, secondTile: Tile, speed: TimeInterval, completionBlock: @escaping () -> Void)
}

final class LogicManager {
    
    var delegate: LogicManagerDelegate?
    var size: Int = defaultBoardDimmension
    var moveEnabled = true
    var boardManager: BoardManager?
    private var tiles = [Tile]()
    
    public func start() {
        initialSetupTiles()
        shuffleInitialTiles()
    }
    
    public func customStart(tiels: [Tile]) {
        customInitialSetupTiles(tiles: tiles)
    }
    
    public func shift(direction: ShiftDirection, speed: TimeInterval = 0.1, completionBlock: @escaping () -> Void) {
        boardManager?.shift(direction: direction, speed: speed, delegate: delegate) {
            completionBlock()
        }
    }
    
    public func performAIFinish(isIDA: Bool) {
        delegate?.didDisableInteractions()
        delegate?.didAIStart()
        DispatchQueue.global(qos: .background).async { [weak self] () -> Void in
            guard let self = self else { return }
            let startTime = Date().timeIntervalSince1970
            let moves = AI(grid: self.getGridCopy(), size: self.size).getMoves(isIDA: isIDA)
            let aiTime = Date().timeIntervalSince1970 - startTime
            print("Calculated \(isIDA ? "IDA" : "BFS"): \(moves.count) moves in \(aiTime)")
            self.delegate?.didAISolve(moves: moves.count)
            DispatchQueue.main.async { () -> Void in
                self.performAIMoves(moves: moves) {
                    self.delegate?.didEnableInteractions()
                }
            }
        }
    }
    
    private func getGridCopy() -> [Tile] {
        return boardManager?.getGridCopy() ?? []
    }
    
    private func initialSetupTiles() {
        tiles = [Tile(position: Position(x: 0, y: 0), value: 1, goalValue: 1),
                 Tile(position: Position(x: 1, y: 0), value: 8, goalValue: 8),
                 Tile(position: Position(x: 2, y: 0), value: 7, goalValue: 7),
                 Tile(position: Position(x: 0, y: 1), value: 2, goalValue: 2),
                 Tile(position: Position(x: 1, y: 1), value: 0, goalValue: 0),
                 Tile(position: Position(x: 2, y: 1), value: 6, goalValue: 6),
                 Tile(position: Position(x: 0, y: 2), value: 3, goalValue: 3),
                 Tile(position: Position(x: 1, y: 2), value: 4, goalValue: 4),
                 Tile(position: Position(x: 2, y: 2), value: 5, goalValue: 5)]
        boardManager = BoardManager(tiles: tiles, size: size)
        boardManager?.refreshNeighborTiles(tiles: tiles)
        delegate?.didSetTiles(tiles: tiles)
    }
    
    private func customInitialSetupTiles(tiles: [Tile]) {
        boardManager = BoardManager(tiles: tiles, size: size)
        boardManager?.refreshNeighborTiles(tiles: tiles)
        delegate?.didSetTiles(tiles: tiles)
    }
    
    private func shuffleInitialTiles() {
        var moves = [ShiftDirection]()
        let possibleMoves: [ShiftDirection] = [.up, .down, .left, .right]
        for _ in 0..<size*60 {
            let randomMove = possibleMoves.randomElement()!
            moves.append(randomMove)
        }
        
        delegate?.didStartShuffle()
        performAIMoves(moves: moves, speed: 0.0001) {
            self.delegate?.didEndShuffle()
        }
    }
    
    private func performAIMoves(moves: [ShiftDirection], speed: TimeInterval = 0.1, completionBlock: @escaping () -> Void) {
        var movesQueue = Queue<ShiftDirection>()
        moves.forEach { movesQueue.enqueue($0) }
        func moveIfPossible() {
            if let moveToPerform = movesQueue.dequeue() as ShiftDirection? {
                shift(direction: moveToPerform, speed: speed, completionBlock: {
                    moveIfPossible()
                })
            } else {
                completionBlock()
            }
        }
        moveIfPossible()
    }
}
