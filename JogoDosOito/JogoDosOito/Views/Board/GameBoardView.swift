//
//  GameBoardView.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import UIKit

final class GameBoardView: UIView {
    // MARK: - Variables
    private var startLocation: CGPoint = CGPoint.zero
}
