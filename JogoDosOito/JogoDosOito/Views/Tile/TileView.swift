//
//  TileView.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation
import UIKit

final class TileView: UIView {
    
    var valueLabel: UILabel!
    var position: Position
    
    init(position: Position, color: UIColor) {
        valueLabel = UILabel()
        valueLabel.textColor = color
        valueLabel.textAlignment = .center
        self.position = position
        super.init(frame: CGRect.zero)
        backgroundColor = .clear
        layer.borderWidth = 2
        layer.borderColor = color.cgColor
        addSubview(valueLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
