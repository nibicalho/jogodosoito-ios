//
//  ShiftDirection.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation

enum ShiftDirection: String {
    case up = "up ↑"
    case right = "right →"
    case down = "down ↓"
    case left = "left ←"
}
