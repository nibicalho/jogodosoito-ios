//
//  Position.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation

struct Position: Equatable {
    var x, y: Int
}
