//
//  ArrayExtension.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation

extension Array where Element: Hashable {
    
    func randomize() -> Array {
        return Array(Set(self))
    }
    
    func randomElement() -> Element? {
        guard !self.isEmpty else {
            return nil
        }
        return self[Int(arc4random_uniform(UInt32(self.count)))]
    }
}
