//
//  DateExtension.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation

extension Date {
    
    func countElapsedTime() -> Int {
        return  Int(round(Date().timeIntervalSince1970 - self.timeIntervalSince1970))
    }
}
