//
//  TimeIntervalExtension.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 13/12/20.
//

import Foundation

extension TimeInterval {
    
    public func stringFromTimeInterval() -> String {
        
        let time = NSInteger(self)
        
        let milliseconds = Int((self.truncatingRemainder(dividingBy: 1)) * 100)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        
        return String(format: "%02d: %02d : %02d", minutes, seconds, milliseconds)
        
    }
}
