//
//  Constants.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation
import UIKit

let defaultBoardDimmension: Int = 3

let availableSizes = [3,4,5,6]

let popupDelay: TimeInterval = 3.0
let finalLevel = 7

let defaultBoardSize: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 600.0 : 300.0
