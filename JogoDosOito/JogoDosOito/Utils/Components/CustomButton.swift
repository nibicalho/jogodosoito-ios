//
//  CustomButton.swift
//  JogoDosOito
//
//  Created by Nicolas Bicalho on 12/12/20.
//

import Foundation
import UIKit

final class CustomButton: UIButton {
    
    override var isEnabled: Bool {
        didSet {
            reactToButton(enabled: isEnabled)
        }
    }

    var borderWidth: CGFloat = 2.0

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }

    private func setup() {
        layer.borderColor = self.currentTitleColor.cgColor
        layer.borderWidth = borderWidth
    }
    
    private func reactToButton(enabled: Bool) {
        if enabled {
            setTitleColor(.systemBlue, for: .normal)
            layer.borderColor = self.currentTitleColor.cgColor
        } else {
            setTitleColor(.lightGray, for: .normal)
            layer.borderColor = UIColor.lightGray.cgColor
        }
    }
}
