## __Atividade do REO 3 - 2020/02 - Inteligência Artificial - GCC128 - UFLA__ ##

---

__Professor:__ Ahmed Ali Abdalla Esmin

__Alunos:__ Nicolas Abrantes Bicalho - 10A / Arlen Mateus Mendes - 14A

---

### Para que serve este repositório? ###

O objetivo deste trabalho é implementar dois métodos de busca (um método cego ou não informado e outro informado, aplicados ao jogo dos oito.

![picture](Imagens/Preview2.png) ![picture](Imagens/Preview1.png)

### Ferramentas e Linguagem ###

* __Linguagem:__ Swift 5
* __SO:__ iOS / macOS Catalina
* __Ferramentas:__ Xcode 12.1 / Git

### Evidências ###

* [Video Abrir Projeto xCode](https://www.loom.com/share/5bafac4b3ba3419a99d68b7f17540df9)
* [Print do arquivo e função a ser atualizado caso queira mudar a entrada de resultado esperado](https://drive.google.com/file/d/1vWnr2eu48tImhyowM2BKVFBq7MRmJiwh/view?usp=sharing)

